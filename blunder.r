source("./main.R");

RES <- readRDS(file = "MS068.rds");

dfBacillusSubtilisPeptide1 <- RES$isotope_patterns_C[
  RES$isotope_patterns_C$cluster == 1,
];
dfBacillusSubtilisPeptide1$maxIntMass <- round(dfBacillusSubtilisPeptide1$maxIntMass);
dfBacillusSubtilisPeptide1$intensity <- round(dfBacillusSubtilisPeptide1$intensity/1000);
dfBacillusSubtilisPeptide1 <- dfBacillusSubtilisPeptide1[, -1*c(1, 2)];

dfBacillusSubtilisPeptide2 <- RES$isotope_patterns_C[
  RES$isotope_patterns_C$cluster == 4, #4
];
dfBacillusSubtilisPeptide2$maxIntMass <- round(dfBacillusSubtilisPeptide2$maxIntMass);
dfBacillusSubtilisPeptide2$intensity <- round(dfBacillusSubtilisPeptide2$intensity/1000);
dfBacillusSubtilisPeptide2 <- dfBacillusSubtilisPeptide2[, -1*c(1, 2)];

iMin <- min(dfBacillusSubtilisPeptide1$maxIntMass, dfBacillusSubtilisPeptide2$maxIntMass);
iMax <- max(dfBacillusSubtilisPeptide1$maxIntMass, dfBacillusSubtilisPeptide2$maxIntMass);

dfS <- data.frame(
  mz = NA,
  S1 = NA,
  S2 = NA
)
for (ii in iMin:iMax) {
  dfP1 <- dfBacillusSubtilisPeptide1[dfBacillusSubtilisPeptide1$maxIntMass == ii,];
  dfP2 <- dfBacillusSubtilisPeptide2[dfBacillusSubtilisPeptide2$maxIntMass == ii,];
  
  rS1 <- 0;
  rS2 <- 0;
  if (nrow(dfP1) == 1) rS1 <- dfP1$intensity;
  if (nrow(dfP2) == 1) rS2 <- dfP2$intensity;
  
  dfS <- rbind(dfS, list(mz = ii, S1 = rS1, S2 = rS2));
}

dfS <- dfS[-1,];
rownames(dfS) <- dfS$mz;
mS <- as.matrix(dfS[, -1]); # Here is S!

mC <- data.frame(
  t = 1:20,
  C1 = sapply(1:20, function(x) {return(chromacurve(x = x, mu = 2, sigma = 1));}),
  C2 = sapply(1:20, function(x) {return(chromacurve(x = x, mu = 16, sigma = 1));})
);

plot_ly( # Complete
  x = ~RES$isotope_patterns_C$maxIntMass,
  y = ~RES$isotope_patterns_C$intensity,
  color = ~as.character(paste("C", RES$isotope_patterns_C$cluster)),
  name= RES$isotope_patterns_C$cluster,
  type = "bar"
) %>%
layout(title = "Isotope Pattern dC original",
   yaxis = list(title = "I"),
   xaxis = list(title = "m/z")
);
plot_ly( # 1
  x = ~dfBacillusSubtilisPeptide1$maxIntMass,
  y = ~dfBacillusSubtilisPeptide1$intensity,
  color = ~as.character(paste("C", dfBacillusSubtilisPeptide1$cluster)),
  name= dfBacillusSubtilisPeptide1$cluster,
  type = "bar"
) %>%
  layout(title = "Isotope Pattern dC 2",
         yaxis = list(title = "I"),
         xaxis = list(title = "m/z")
);

plot_ly( # 2
  x = ~dfBacillusSubtilisPeptide2$maxIntMass,
  y = ~dfBacillusSubtilisPeptide2$intensity,
  color = ~as.character(paste("C", dfBacillusSubtilisPeptide2$cluster)),
  name= dfBacillusSubtilisPeptide2$cluster,
  type = "bar"
) %>%
layout(title = "Isotope Pattern dC 2",
       yaxis = list(title = "I"),
       xaxis = list(title = "m/z")
);

plot_ly( # Both
  x = ~rbind(dfBacillusSubtilisPeptide1, dfBacillusSubtilisPeptide2)$maxIntMass,
  y = ~rbind(dfBacillusSubtilisPeptide1, dfBacillusSubtilisPeptide2)$intensity,
  color = ~as.character(paste("C", rbind(dfBacillusSubtilisPeptide1, dfBacillusSubtilisPeptide2)$cluster)),
  name= rbind(dfBacillusSubtilisPeptide1, dfBacillusSubtilisPeptide2)$cluster,
  type = "bar"
) %>%
layout(title = "Isotope Pattern dC 2",
   yaxis = list(title = "I"),
   xaxis = list(title = "m/z")
);

dfPlot <- data.frame(
  Time = c(mC$t, mC$t),
  Peptide = c(mC$C1, mC$C2),
  Species = as.character(c(vector(mode = "numeric", length = nrow(mC)) + 1, vector(mode = "numeric", length = nrow(mC)) + 2))
);
pl <- ggplot(data = dfPlot, aes(x = Time, y = Peptide, group = Species)) +
  geom_line(aes(color = Species)) + # linetype = Species
  geom_point(size = 0)
pl <- pl + scale_color_manual(values=c("red", "blue"))
pl;
